﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xplat.Net.Mail;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            ImapClient client = new ImapClient();
            client.MessageRecived += client_MessageRecived;
            client.MessageSend += client_MessageSend;
            
            Console.ReadKey();
        }

        static void client_MessageSend(string line)
        {
            Console.WriteLine("C: {0}", line);
        }

        static void client_MessageRecived(string line)
        {
            Console.WriteLine("S: {0}", line);
        }
    }
}
