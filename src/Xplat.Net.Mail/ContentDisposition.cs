﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xplat.Net.Mail
{
    public static partial class Rfc
    {
        /// <summary>
        /// Parser for ContentDisposition <a href="https://tools.ietf.org/html/rfc2183">RFC 2183</a>
        /// Standart class in .NET contains some bugs, instead this, we are using self parsing
        /// </summary>
        public static class ContentDisposition
        {
            public static System.Net.Mime.ContentDisposition Parse(string headerValue)
            {
                if (headerValue == null)
                    throw new ArgumentNullException("headerValue");

                // Create empty ContentDisposition - we will fill in details as we read them
                var contentDisposition = new System.Net.Mime.ContentDisposition();

                // Now decode the parameters
                List<KeyValuePair<string, string>> parameters = ParameterValuesHeader.Parse(headerValue);

                foreach (KeyValuePair<string, string> keyValuePair in parameters)
                {
                    string key = keyValuePair.Key.ToUpperInvariant().Trim();
                    string value = RemoveQuotesIfAny(keyValuePair.Value.Trim());
                    switch (key)
                    {
                        case "":
                            // This is the DispisitionType - it has no key since it is the first one
                            // and has no = in it.
                            contentDisposition.DispositionType = value;
                            break;

                        // The correct name of the parameter is filename, but some emails also contains the parameter
                        // name, which also holds the name of the file. Therefore we use both names for the same field.
                        case "NAME":
                        case "FILENAME":
                            // The filename might be in qoutes, and it might be encoded-word encoded
                            contentDisposition.FileName = EncodedWord.Decode(value);
                            break;
                        case "CREATION-DATE":
                            // Notice that we need to SpecifyKind for DateTime because of a failure in .NET 2.0.
                            // The failure is: you cannot give contentDisposition a DateTime with a Kind of UTC
                            // It will set the CreationDate correctly, but when trying to read it out it will throw an exception.
                            // It is the same with ModificationDate and ReadDate.
                            // This is fixed in 4.0 - maybe in 3.0 too.
                            DateTime creationDate = DateTime.SpecifyKind(DateTimeHeader.Parse(value), DateTimeKind.Unspecified);
                            contentDisposition.CreationDate = creationDate;
                            break;
                        case "MODIFICATION-DATE":
                            DateTime midificationDate = DateTime.SpecifyKind(DateTimeHeader.Parse(value), DateTimeKind.Unspecified);
                            contentDisposition.ModificationDate = midificationDate;
                            break;
                        case "READ-DATE":
                            DateTime readDate = DateTime.SpecifyKind(DateTimeHeader.Parse(value), DateTimeKind.Unspecified);
                            contentDisposition.ReadDate = readDate;
                            break;
                        case "SIZE":
                            contentDisposition.Size = SizeParser.Parse(value);
                            break;
                        case "CHARSET": // ignoring invalid parameter in Content-Disposition
                            break;
                        default:
                            if (key.StartsWith("X-"))
                            {
                                contentDisposition.Parameters.Add(key, value);
                                break;
                            }

                            throw new ArgumentException("Unknown parameter in Content-Disposition. Ask developer to fix! Parameter: " + key);
                    }
                }

                return contentDisposition;
            }
        }
    }
}
