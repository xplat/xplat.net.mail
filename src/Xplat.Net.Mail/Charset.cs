﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Xplat.Net.Mail
{
    public static partial class Rfc
    {
        /// <summary>
        /// Utility class used for mapping from a characterSet to an <see cref="Encoding"/>.<br/>
        /// <br/>
        /// The functionality of the class can be altered by adding mappings
        /// using <see cref="AddMapping"/>.<br/>
        /// <br/>
        /// Given a characterSet, it will try to find the Encoding as follows:
        /// <list type="number">
        ///     <item>
        ///         <description>If a mapping for the characterSet was added, use the specified Encoding from there. Mappings can be added using <see cref="AddMapping"/>.</description>
        ///     </item>
        ///     <item>
        ///         <description>Try to parse the characterSet and look it up using <see cref="Encoding.GetEncoding(int)"/> for codepages or <see cref="Encoding.GetEncoding(string)"/> for named encodings.</description>
        ///     </item>
        /// </list>
        /// </summary>
        public static class Charset
        {
            private static Dictionary<string, Encoding> _incorrectNames;

            static Charset()
            {
                _incorrectNames = new Dictionary<string, Encoding>();

                AddMapping("utf8", Encoding.UTF8);
                AddMapping("binary", Encoding.ASCII);
            }

            /// <summary>
            /// Parses a character set into an encoding.
            /// </summary>
            /// <param name="characterSet">The character set to parse</param>
            /// <returns>An encoding which corresponds to the character set</returns>
            /// <exception cref="ArgumentNullException">If <paramref name="characterSet"/> is <see langword="null"/></exception>
            public static Encoding GetEncoding(string characterSet)
            {
                if (characterSet == null)
                    throw new ArgumentNullException("characterSet");

                string charSetUpper = characterSet.ToUpperInvariant();

                // Check if the characterSet is explicitly mapped to an encoding
                if (_incorrectNames.ContainsKey(charSetUpper))
                    return _incorrectNames[charSetUpper];

                if (charSetUpper.Contains("WINDOWS") || charSetUpper.Contains("CP"))
                {
                    // It seems the characterSet contains an codepage value, which we should use to parse the encoding
                    charSetUpper = charSetUpper.Replace("CP", ""); // Remove cp
                    charSetUpper = charSetUpper.Replace("WINDOWS", ""); // Remove windows
                    charSetUpper = charSetUpper.Replace("-", ""); // Remove - which could be used as cp-1554

                    // Now we hope the only thing left in the characterSet is numbers.
                    int codepageNumber = int.Parse(charSetUpper, CultureInfo.InvariantCulture);

                    return Encoding.GetEncoding(codepageNumber);
                }

                // It seems there is no codepage value in the characterSet. It must be a named encoding

                //Handle missing hyphen after ISO, for example ISO8859-15 instead ISO-8859-15
                if (charSetUpper.Length > 3 && charSetUpper.StartsWith("ISO") && charSetUpper[3] >= '0' && charSetUpper[3] <= '9')
                    characterSet = "iso-" + characterSet.Substring(3);

                return Encoding.GetEncoding(characterSet);
            }

            /// <summary>
            /// Puts a mapping from <paramref name="characterSet"/> to <paramref name="encoding"/>
            /// into the <see cref="Charset"/>'s internal mapping Dictionary.
            /// </summary>
            /// <param name="characterSet">The string that maps to the <paramref name="encoding"/></param>
            /// <param name="encoding">The <see cref="Encoding"/> that should be mapped from <paramref name="characterSet"/></param>
            /// <exception cref="ArgumentNullException">If <paramref name="characterSet"/> is <see langword="null"/></exception>
            /// <exception cref="ArgumentNullException">If <paramref name="encoding"/> is <see langword="null"/></exception>
            public static void AddMapping(string characterSet, Encoding encoding)
            {
                if (characterSet == null)
                    throw new ArgumentNullException("characterSet");

                if (encoding == null)
                    throw new ArgumentNullException("encoding");

                _incorrectNames.Add(characterSet.ToUpperInvariant(), encoding);
            }
        }
    }
}
