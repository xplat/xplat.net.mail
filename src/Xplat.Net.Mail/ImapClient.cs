﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Security.Cryptography;

namespace Xplat.Net.Mail
{
    public class ImapClient
    {
        private ulong _commandCount = default(ulong);

        private string GetNextCommandPrefix()
        {
            _commandCount++;
            string commandPrefix = string.Format("A{0:000}", _commandCount);
            return commandPrefix;
        }

        private TcpClientForImap _imapClientTcp;

        private Regex serverGreetingRegex = new Regex(@"^*\s+(OK|PREAUTH|BYE)\s+(.*)$");

        public event Action<string> MessageRecived;
        public event Action<string> MessageSend;

        protected void OnMessageRecived(string message)
        {
            if (MessageRecived == null)
                return;
            MessageRecived(message);
        }

        protected void OnMessageSend(string message)
        {
            if (MessageSend == null)
                return;
            MessageSend(message);
        }

        public ImapClientState Connect(string hostname)
        {
            return Connect(hostname, 143, false);
        }

        public ImapClientState Connect(string hostname, int port, bool useSsl)
        {
            _imapClientTcp = new TcpClientForImap(hostname, port, useSsl);
            _imapClientTcp.MessageRecived += OnMessageRecived;
            _imapClientTcp.MessageSend += OnMessageSend;

            _commandCount = 0;

            string serverGreeting = _imapClientTcp.ReadLine();

            Match serverGreetingMath = serverGreetingRegex.Match(serverGreeting);
            if (!serverGreetingMath.Success)
                throw new Exception(string.Format("Ответ сервера '{0}' не соответсвует формату IMAP сервера", serverGreeting));
            switch (serverGreetingMath.Groups[1].Value)
            {
                case "OK":
                    CurrentState = ImapClientState.NotAuthenticated;
                    break;
                case "PREAUTH":
                    CurrentState = ImapClientState.Authenticated;
                    break;
                case "BYE":
                    CurrentState = ImapClientState.Logout;
                    break;
                default:
                    throw new Exception(string.Format("Неверный отклик IMAP сервера - '{0}'", serverGreeting));
            }

            if (CurrentState != ImapClientState.Logout)
                this.Capability();

            return _currentState;
        }

        public void Close()
        {
            if (_currentState != ImapClientState.Selected)
                return; //throw new InvalidOperationException("Текущее состояние IMAP клиента не позволяет вызывать данную команду.");

            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} CLOSE", commandPrefix);
            _imapClientTcp.WriteLine(command);

            do { _imapClientTcp.ReadLine(); }
            while (!_imapClientTcp.LastString.StartsWith(commandPrefix));

            if (_imapClientTcp.LastString.Contains(" OK "))
                CurrentState = ImapClientState.Authenticated;
            else
                throw new Exception(_imapClientTcp.LastString);
        }

        public void Select(string mailboxName)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} SELECT \"{1}\"", commandPrefix, mailboxName);
            _imapClientTcp.WriteLine(command);

            Mailbox mailbox = new Mailbox { Name = mailboxName };

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();

                if (nextLine.StartsWith("*"))
                {
                    if (nextLine.EndsWith("EXISTS"))
                        mailbox.Exists = int.Parse(nextLine.Substring(2, nextLine.Length - 8), CultureInfo.InvariantCulture);
                    else if (nextLine.EndsWith("RECENT"))
                        mailbox.Recent = int.Parse(nextLine.Substring(2, nextLine.Length - 8), CultureInfo.InvariantCulture);
                }

                //if (nextLine[0] == '*')
                //{
                //    if (nextLine.StartsWith("* FLAGS "))
                //    { }
                //    else if ()
                //}

//С А142 SELECT INBOX 
//S * 172 EXISTS 
//S * 1 RECENT
//S * OK [UNSEEN 12) Message 12 is first unseen
//S * OK [UIDVALIDITY 3857529045] UIDs valid 
//S * FLAGS (\Answered \Flagged \Deleted \Seen \Draft) 
//S * OK [PERMANENTFLAGS (\Deleted \Seen \*)] Limited 
//S A142 OK [READ-WRITE] SELECT completed
            }
            while (!nextLine.StartsWith(commandPrefix));

            nextLine = nextLine.Substring(commandPrefix.Length + 1);
            if (nextLine.StartsWith("OK"))
                _currentState = ImapClientState.Selected;
        }

        public IEnumerable<ulong> Search(ImapSearch imapSearch)
        {
            string commandPrefix = GetNextCommandPrefix();

            if (imapSearch.IsASCII())
            {
                string command = string.Format(CultureInfo.InvariantCulture, @"{0} UID SEARCH {1}", commandPrefix, imapSearch);
                _imapClientTcp.WriteLine(command);
            }
            else
            {
                string[] data = imapSearch.GetNonASCII();

                string command = string.Format(CultureInfo.InvariantCulture, @"{0} UID SEARCH {1}", commandPrefix, data[0]);
                _imapClientTcp.WriteLine(command);

                for (int i = 1; i < data.Length; i++)
                    _imapClientTcp.WriteLine(data[i]);
            }            

            StringBuilder stringBuilder = new StringBuilder();
            string nextLine = string.Empty;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
                if (nextLine.StartsWith(commandPrefix))
                    continue;
                stringBuilder.Append(nextLine);
            }
            while (!nextLine.StartsWith(commandPrefix));

            Regex regex = new Regex(@"\* SEARCH((\s+\d+)+)");
            Match searchMatch = regex.Match(stringBuilder.ToString());
            if (searchMatch.Success)
            {
                MatchCollection matchCollection = Regex.Matches(searchMatch.Groups[1].Value, @"\d+");
                foreach (Match match in matchCollection)
                    yield return ulong.Parse(match.Value);
            }
        }

        public IEnumerable<MailMessage> SearchAndGet(ImapSearch imapSearch)
        {
            foreach (ulong uid in Search(imapSearch))
                yield return Fetch(uid);
        }

        public void List()
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format(@"{0} LIST ""/"" *", commandPrefix);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
                //С А142 SELECT INBOX 
                //S * 172 EXISTS 
                //S * 1 RECENT
                //S * OK [UNSEEN 12) Message 12 is first unseen
                //S * OK [UIDVALIDITY 3857529045] UIDs valid 
                //S * FLAGS (\Answered \Flagged \Deleted \Seen \Draft) 
                //S * OK [PERMANENTFLAGS (\Deleted \Seen \*)] Limited 
                //S A142 OK [READ-WRITE] SELECT completed
            }
            while (!nextLine.StartsWith(commandPrefix));
        }

        #region Client Commands - The following commands are valid in any state

        private List<string> _capability;

        /// <summary>
        /// Requests a listing of capabilities that the server supports.
        /// </summary>
        public void Capability()
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} CAPABILITY", commandPrefix);
            _imapClientTcp.WriteLine(command);

            string nextLine = string.Empty;
            do
            {
                nextLine = _imapClientTcp.ReadLine();

                if (nextLine.StartsWith("* CAPABILITY"))
                {
                    string capability = nextLine.Substring(13);
                    _capability = new List<string>(capability.Split(' '));
                }
            }
            while (!nextLine.StartsWith(commandPrefix));
        }

        /// <summary>
        /// Since any command can return a status update as untagged data, the 
        /// NOOP command can be used as a periodic poll for new messages or 
        /// message status updates during a period of inactivity (this is the 
        /// preferred method to do this).  The NOOP command can also be used 
        /// to reset any inactivity autologout timer on the server.
        /// </summary>
        public void Noop()
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} NOOP", commandPrefix);
            _imapClientTcp.WriteLine(command);

            string nextLine = string.Empty;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));
        }

        /// <summary>
        /// The LOGOUT command informs the server that the client is done with the connection.
        /// The server MUST send a BYE untagged response before the (tagged) OK response, and then close the network connection.
        /// </summary>
        public void Logout()
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} LOGOUT", commandPrefix);
            _imapClientTcp.WriteLine(command);

            string nextLine = string.Empty;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));

            _currentState = ImapClientState.Logout;
        }

        #endregion

        #region Client Commands - Not Authenticated State

        public ImapClientState CramMD5(string login, string password)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} AUTHENTICATE CRAM-MD5", commandPrefix);
            _imapClientTcp.WriteLine(command);

            //{1} {2}

            byte[] challenge = Convert.FromBase64String(_imapClientTcp.ReadLine().Substring(2));
            HMACMD5 hmac = new HMACMD5(Encoding.ASCII.GetBytes(password));
            byte[] encrypted = hmac.ComputeHash(challenge);
            StringBuilder sb = new StringBuilder(login).Append(" ");
            foreach (byte b in encrypted)
                sb.Append(b.ToString("x2"));

            _imapClientTcp.WriteLine(Convert.ToBase64String(Encoding.ASCII.GetBytes(sb.ToString())));

            string nextLine = string.Empty;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));

            nextLine = nextLine.Substring(commandPrefix.Length + 1);

            if (nextLine.StartsWith("OK"))
            {
                CurrentState = ImapClientState.Authenticated;
            }
            else if (nextLine.StartsWith("NO"))
            {
                CurrentState = ImapClientState.NotAuthenticated;            
            }
            else if (nextLine.StartsWith("BAD"))
            {
                CurrentState = ImapClientState.NotAuthenticated;
            }

            return _currentState;
        }

        public ImapClientState Login(string login, string password)
        {
            if (_capability.Contains("LOGINDISABLED"))
            {
                if (_capability.Contains("AUTH=CRAM-MD5"))
                    return CramMD5(login, password);

                return ImapClientState.NotAuthenticated;
            }

            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} LOGIN {1} {2}", commandPrefix, login, password);
            _imapClientTcp.WriteLine(command);

            string nextLine = string.Empty;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));

            nextLine = nextLine.Substring(commandPrefix.Length + 1);

            if (nextLine.StartsWith("OK"))
            {
                CurrentState = ImapClientState.Authenticated;
            }
            else if (nextLine.StartsWith("NO"))
            {
                CurrentState = ImapClientState.NotAuthenticated;
            }
            else if (nextLine.StartsWith("BAD"))
            {
                CurrentState = ImapClientState.NotAuthenticated;
            }

            return ImapClientState.Authenticated;
        }

        #endregion

        public void Store()
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format(@"{0} UID STORE 2 +FLAGS (\Deleted)", commandPrefix);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));
        }

        public void MarkUnseen(ulong uid)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format(@"{0} UID STORE {1} -FLAGS.SILENT (\Seen)", commandPrefix, uid);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));

        }

        public void Delete(ulong uid)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format(@"{0} UID STORE {1} +FLAGS (\Deleted)", commandPrefix, uid);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));
            
        }

        public void Create(string mailboxName)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} CREATE \"{1}\"", commandPrefix, mailboxName);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));
        }

        public void Copy(ulong uid, string mailboxName)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} UID COPY {1} \"{2}\"", commandPrefix, uid, mailboxName);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
            }
            while (!nextLine.StartsWith(commandPrefix));
        }

        public void Expunge()
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} EXPUNGE", commandPrefix);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();                
            }
            while (!nextLine.StartsWith(commandPrefix));
        }

        public MailMessage Fetch(ulong uid)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} UID FETCH {1} BODY[]", commandPrefix, uid);
            _imapClientTcp.WriteLine(command);

            StringBuilder emlBuilder = null;
            int needLength = 0; 

            string nextLine = string.Empty;
            while(!nextLine.StartsWith(commandPrefix + " "))
            {
                nextLine = _imapClientTcp.ReadLine();

                if (nextLine.StartsWith(commandPrefix + " "))
                    break;
                if (nextLine.StartsWith("*"))
                {
                    if (Regex.IsMatch(nextLine, @"^* \d+ FETCH "))
                    {
                        Match match = Regex.Match(nextLine, @"BODY\[\] \{(\d+)\}$");

                        if (match.Success)
                        {
                            if (emlBuilder != null)
                            {
                                //emlBuilder.Remove(emlBuilder.Length - 1, 1);
                                throw new Exception("Более одного письма");
                                //return MailMessageParse.FromEml(emlBuilder.ToString());
                            }

                            emlBuilder = new StringBuilder();
                            needLength = int.Parse(match.Groups[1].Value);
                        }
                    }
                }
                else
                {
                    if (emlBuilder != null && emlBuilder.Length < needLength)
                        emlBuilder.AppendLine(nextLine);
                }
            }

            return MailMessageParse.FromEml(emlBuilder.ToString());
        }

        public void Examine(string mailbox)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} EXAMINE {1}", commandPrefix, mailbox);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
                //С А142 SELECT INBOX 
                //S * 172 EXISTS 
                //S * 1 RECENT
                //S * OK [UNSEEN 12) Message 12 is first unseen
                //S * OK [UIDVALIDITY 3857529045] UIDs valid 
                //S * FLAGS (\Answered \Flagged \Deleted \Seen \Draft) 
                //S * OK [PERMANENTFLAGS (\Deleted \Seen \*)] Limited 
                //S A142 OK [READ-WRITE] SELECT completed
            }
            while (!nextLine.StartsWith(commandPrefix));

            _currentState = ImapClientState.Selected;
        }

        public void Status(string mailbox)
        {
            string commandPrefix = GetNextCommandPrefix();
            string command = string.Format("{0} STATUS \"{1}\" (UIDVALIDITY)", commandPrefix, mailbox);
            _imapClientTcp.WriteLine(command);

            string nextLine;
            do
            {
                nextLine = _imapClientTcp.ReadLine();
                //С А142 SELECT INBOX 
                //S * 172 EXISTS 
                //S * 1 RECENT
                //S * OK [UNSEEN 12) Message 12 is first unseen
                //S * OK [UIDVALIDITY 3857529045] UIDs valid 
                //S * FLAGS (\Answered \Flagged \Deleted \Seen \Draft) 
                //S * OK [PERMANENTFLAGS (\Deleted \Seen \*)] Limited 
                //S A142 OK [READ-WRITE] SELECT completed
            }
            while (!nextLine.StartsWith(commandPrefix));

            _currentState = ImapClientState.Selected;
        }        

        private ImapClientState _currentState = ImapClientState.Logout;

        event Action<ImapClientState> CurrentStateChange;

        public ImapClientState CurrentState
        {
            get { return _currentState; }
            private set 
            {
                if (_currentState != value)
                {
                    _currentState = value;
                    if (CurrentStateChange != null)
                        CurrentStateChange(_currentState);
                }
            }
        }
    }
}
