﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace Xplat.Net.Mail
{
    internal class TcpClientForImap : TcpClient
    {
        private bool SertificateValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;
            return false;
        }

        private Stream writeStream;
        private StreamReader readStream;

        public TcpClientForImap(string hostName, int port, bool useSsl)
            : base(hostName, port)
        {
            writeStream = this.GetStream();


            if (useSsl)
            {
                SslStream sslStream = new SslStream(writeStream, false, SertificateValidation);
                sslStream.AuthenticateAsClient(hostName);
                writeStream = sslStream;
            }
            readStream = new StreamReader(writeStream, Encoding.GetEncoding(1251));
        }

        Regex _notRegex = new Regex("[^0-9a-zA-Z,+&-@\" ]+");

        public void WriteLine(string message)
        {
            OnMessageSend(message);

            byte[] data = Encoding.UTF8.GetBytes(message + "\r\n");
                        
            writeStream.Write(data, 0, data.Length);
        }

        Regex decodeRegex = new Regex("&([0-9a-zA-Z,+]+)-");
        Regex base64Regex = new Regex(@"(R\w|Q\d|Q[hl])$");
        Encoding usc2be = Encoding.GetEncoding("UnicodeFFFE");

        private Dictionary<Encoding, StreamReader> streamReaders = new Dictionary<Encoding, StreamReader>();

        private string _lastString = null;

        public string LastString
        {
            get { return _lastString; }
        }

        public string ReadLine(Encoding encoding)
        {
            if (!streamReaders.ContainsKey(encoding))
                streamReaders.Add(encoding, new StreamReader(writeStream, encoding));
            
            StreamReader streamReader = streamReaders[encoding];

            string recivedMessage = streamReader.ReadLine();
            OnMessageRecived(recivedMessage);
            return recivedMessage;
        }

        public string ReadLine()
        {
            string recivedMessage = readStream.ReadLine();
            
            while (decodeRegex.IsMatch(recivedMessage))
            {
                Match match = decodeRegex.Match(recivedMessage);
                string base64String = match.Groups[1].Value.Replace(',', '/');
                if (!base64Regex.IsMatch(base64String))
                    base64String = base64String.Length % 2 == 0 ? base64String + "==" : base64String + "=";
                string normalString = usc2be.GetString(Convert.FromBase64String(base64String));
                recivedMessage = recivedMessage.Replace(match.Groups[0].Value, normalString);
            }

            OnMessageRecived(recivedMessage);
            return recivedMessage;
        }

        public event Action<string> MessageSend;

        protected void OnMessageSend(string message)
        {
            if (MessageSend != null)
                MessageSend(message);
        }

        public event Action<string> MessageRecived;

        protected void OnMessageRecived(string message)
        {
            _lastString = message;
            if (MessageRecived != null)
                MessageRecived(message);
        }
    }
}
