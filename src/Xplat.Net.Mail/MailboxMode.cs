﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xplat.Net.Mail
{
    public enum MailboxMode
    {
        ReadWrite,
        ReadOnly
    }
}
