﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Xplat.Net.Mail
{
    public static partial class Rfc
    {
        /// <summary>
        /// Class for parse Encoded-Word <a href="http://tools.ietf.org/html/rfc2047">RFC 2047</a>
        /// The form is: "=?charset?encoding?encoded text?=".
        ///   charset - may be any character set registered with IANA. Typically it would be the same charset as the message body.
        ///   encoding - can be either "Q" denoting Q-encoding that is similar to the quoted-printable encoding, or "B" denoting base64 encoding.
        ///   encoded text - is the Q-encoded or base64-encoded text.
        /// An encoded-word may not be more than 75 characters long, including charset, encoding, encoded text, and delimiters. 
        /// If it is desirable to encode more text than will fit in an encoded-word of 75 characters, 
        /// multiple encoded-words (separated by CRLF SPACE) may be used.
        /// </summary>
        public static class EncodedWord
        {
            private static string _encodedWordRegexPattern = @"\=\?([^?]+)\?(Q|B)\?([^?]+)\?\=";
            private static Regex encodedWordRegex = new Regex(_encodedWordRegexPattern, RegexOptions.IgnoreCase);
            private static Regex spacesRegex = new Regex(@"(?<first>" + _encodedWordRegexPattern + @")\s+(?<second>" + _encodedWordRegexPattern + ")", RegexOptions.IgnoreCase);

            /// <summary>
            /// Decode Encoded-Word's in Header value
            /// </summary>
            /// <param name="encodedWord">Header value with Encoded-Word</param>
            /// <returns>decoded Header value</returns>
            public static string Decode(string encodedWord)
            {
                if (string.IsNullOrEmpty(encodedWord))
                    return encodedWord;

                // Any amount of linear-space-white between encoded-word's, is ignored for the purposes of display.
                // http://tools.ietf.org/html/rfc2047#page-12
                // twice replace for correctly replace more than 2 encoded-word's in sequence
                encodedWord = spacesRegex.Replace(encodedWord, "${first}${second}");
                encodedWord = spacesRegex.Replace(encodedWord, "${first}${second}");

                return Regex.Replace(encodedWord, encodedWordRegex.ToString(), match =>
                {
                    Encoding encoding = Charset.GetEncoding(match.Groups[1].Value); // Get encoding by charset
                    if (match.Groups[2].Value[0].ToString().ToUpper() == "B") // Base-64
                        return encoding.GetString(Convert.FromBase64String(match.Groups[3].Value));
                    else //if (match.Groups[2].Value[0].ToString().ToUpper() == "Q") QuotedPrintable
                        return QuotedPrintable.Decode(match.Groups[3].Value.Replace('_', ' '), encoding);
                }, RegexOptions.IgnoreCase);
            }
        }
    }
}
