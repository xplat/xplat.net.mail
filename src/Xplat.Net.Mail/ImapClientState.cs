﻿namespace Xplat.Net.Mail
{
    public enum ImapClientState
    {
        Logout,
        NotAuthenticated,
        Authenticated,
        Selected
    }
}
