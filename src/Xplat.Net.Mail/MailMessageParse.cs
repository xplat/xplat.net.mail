﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

namespace Xplat.Net.Mail
{
    public class MailMessageParse
    {
        private MailMessage _mailMessage;

        public MailMessage MailMessage
        {
            get { return _mailMessage; }
        }

        public delegate void Action<T1, T2>(T1 obj1, T2 obj2);

        private class UnexpectedEndOfMailMessageException : Exception
        { }

        private class MailMessageReader
        {
            public MailMessageReader(IEnumerator<string> enumerator, string boundary)
                : this(enumerator)
            {
                rootBoundary = boundary;
            }

            public MailMessageReader(IEnumerator<string> enumerator)
            {
                this.enumerator = enumerator;

                if (!enumerator.MoveNext())
                    throw new UnexpectedEndOfMailMessageException();
            }

            private IEnumerator<string> enumerator;
            private ContentType _contentType;
            private ContentDisposition _contentDisposition;
            private TransferEncoding _transferEncoding = TransferEncoding.Unknown;

            private string rootBoundary;

            public event Action<string, string> InlineBody;

            private void OnInlineBody(string contentType, string body)
            {
                if (InlineBody != null)
                    InlineBody(contentType, body);
            }

            public event Action<Attachment> Attachment;

            private void OnAttachment(Attachment attachment)
            {
                if (Attachment != null)
                    Attachment(attachment);
            }

            public event Action<string, string> UnknownHeader;

            private void OnUnknownHeader(string headerName, string headerValue)
            {
                if (UnknownHeader != null)
                    UnknownHeader(headerName, headerValue);
            }

            public void ReadHeader()
            {
                while (!string.IsNullOrEmpty(enumerator.Current.TrimEnd()))
                {
                    string headerField = enumerator.Current;
                    enumerator.MoveNext();

                    while (enumerator.Current.StartsWith("\t") || enumerator.Current.StartsWith(" "))
                    {
                        headerField += enumerator.Current.Substring(1); // 1-ый символ \t или пробел не нужен
                        enumerator.MoveNext();
                    }

                    string[] nameValue = headerField.Split(new[] { ':' }, 2);

                    if (nameValue.Length == 2)
                    {
                        string headerName = nameValue[0];
                        string headerValue = nameValue[1].Trim();

                        if (!string.IsNullOrEmpty(headerValue))
                        {
                            switch (headerName.Trim().ToUpperInvariant())
                            {
                                case "CONTENT-TYPE":
                                    headerValue = headerValue.Replace("charset =", "charset=");
                                    _contentType = Rfc.ContentType.Parse(headerValue);
                                    break;
                                case "CONTENT-DISPOSITION":
                                    _contentDisposition = Rfc.ContentDisposition.Parse(headerValue);
                                    break;
                                case "CONTENT-TRANSFER-ENCODING":
                                    switch (headerValue.Trim().ToUpperInvariant())
                                    {
                                        case "BASE64":
                                            _transferEncoding = TransferEncoding.Base64; break;
                                        case "7BIT":
                                            _transferEncoding = TransferEncoding.SevenBit; break;
                                        case "QUOTED-PRINTABLE":
                                            _transferEncoding = TransferEncoding.QuotedPrintable; break;
                                        default:
                                            _transferEncoding = TransferEncoding.Unknown; break;
                                    }
                                    break;
                                default:
                                    OnUnknownHeader(headerName, headerValue);
                                    break;
                            }
                        }
                    }
                }

                enumerator.MoveNext(); // перенос строки для отделения header от body
            }

            private byte[] ReadBase64(string endLine)
            {
                StringBuilder bodyBuilder = new StringBuilder();
                while (!enumerator.Current.StartsWith(endLine))
                {
                    bodyBuilder.Append(enumerator.Current);
                    if (!enumerator.MoveNext())
                        break;
                }
                return Convert.FromBase64String(bodyBuilder.ToString());
            }

            public byte[] ReadSimpleBytes(string endLine)
            {
                StringBuilder bodyBuilder = new StringBuilder();

                while (!enumerator.Current.StartsWith(endLine))
                {
                    bodyBuilder.AppendLine(enumerator.Current);
                    if (!enumerator.MoveNext())
                        break;
                }

                return Encoding.GetEncoding(1251).GetBytes(bodyBuilder.ToString());
            }

            private byte[] ReadQuotedPrintable(string endLine)
            {
                StringBuilder bodyBuilder = new StringBuilder();
                while (!enumerator.Current.StartsWith(endLine))
                {
                    if (enumerator.Current.EndsWith("=")) // Если заканчивается на =, значит есть продолжение строчки
                        bodyBuilder.Append(enumerator.Current.Substring(0, enumerator.Current.Length - 1));
                    else // иначе конец строчки, нужен перевод строки
                        bodyBuilder.AppendLine(enumerator.Current);
                    if (!enumerator.MoveNext())
                        break;
                }

                string quotedString = bodyBuilder.ToString();
                Encoding encoding = Rfc.Charset.GetEncoding(_contentType.CharSet ?? "windows-1251");

                quotedString = Rfc.QuotedPrintable.Decode(quotedString, encoding);

                return encoding.GetBytes(quotedString);
            }

            public void Read()
            {
                ReadHeader();

                string endLine = string.IsNullOrEmpty(rootBoundary) ? ")" : string.Format("--{0}", rootBoundary);

                if (string.IsNullOrEmpty(_contentType.Boundary))
                {
                    byte[] data;

                    switch (_transferEncoding)
                    {
                        case TransferEncoding.Base64:
                            data = ReadBase64(endLine);
                            break;
                        case TransferEncoding.QuotedPrintable:
                            data = ReadQuotedPrintable(endLine);
                            break;
                        case TransferEncoding.SevenBit:
                        case TransferEncoding.Unknown:
                        default:
                            data = ReadSimpleBytes(endLine);
                            break;
                    }

                    if (_contentDisposition == null || _contentDisposition.Inline)
                    {
                        if (_contentType.MediaType.ToUpperInvariant() == MediaTypeNames.Text.Plain.ToUpperInvariant() ||
                            _contentType.MediaType.ToUpperInvariant() == MediaTypeNames.Text.Html.ToUpperInvariant())
                        {
                            Encoding encoding = Encoding.GetEncoding(1251);
                            if (!string.IsNullOrEmpty(_contentType.CharSet))
                                encoding = Rfc.Charset.GetEncoding(_contentType.CharSet);

                            OnInlineBody(_contentType.MediaType, encoding.GetString(data));
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(_contentType.Name))
                            {
                                // TODO: другая поебень
                                Attachment attachment = new Attachment(new MemoryStream(data), _contentType);
                                attachment.ContentDisposition.FileName = _contentType.Name;
                                OnAttachment(attachment);
                            }
                            
                        }
                    }
                    else // Attachment
                    {
                        if (_contentDisposition.DispositionType.ToUpperInvariant() == DispositionTypeNames.Attachment.ToUpperInvariant())
                        {
                            Attachment attachment = new Attachment(new MemoryStream(data), _contentType);

                            attachment.ContentDisposition.CreationDate = _contentDisposition.CreationDate;
                            if (!String.IsNullOrEmpty(_contentDisposition.FileName))
                                attachment.ContentDisposition.FileName = _contentDisposition.FileName;
                            attachment.ContentDisposition.ModificationDate = _contentDisposition.ModificationDate;
                            attachment.ContentDisposition.ReadDate = _contentDisposition.ReadDate;
                            attachment.ContentDisposition.Size = _contentDisposition.Size;
                            if (!String.IsNullOrEmpty(_contentDisposition.FileName))
                                attachment.Name = _contentDisposition.FileName;

                            OnAttachment(attachment);
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(_contentType.Name))
                            {
                                // TODO: другая поебень
                                Attachment attachment = new Attachment(new MemoryStream(data), _contentType);
                                attachment.ContentDisposition.FileName = _contentType.Name;
                                OnAttachment(attachment);
                            }
                        }
                    }
                }
                else
                {
                    endLine = string.Format("--{0}--", _contentType.Boundary);
                    bool notEnd = true;
                    while (!enumerator.Current.StartsWith(endLine) && notEnd)
                    {
                        string startBoundary = string.Format("--{0}", _contentType.Boundary);
                        while (enumerator.Current.Trim() != startBoundary && notEnd)
                            notEnd = enumerator.MoveNext();

                        try
                        {
                            MailMessageReader mailMessageReader = new MailMessageReader(enumerator, _contentType.Boundary);
                            mailMessageReader.UnknownHeader += OnUnknownHeader;
                            mailMessageReader.InlineBody += OnInlineBody;
                            mailMessageReader.Attachment += OnAttachment;
                            mailMessageReader.Read();
                        }
                        catch (UnexpectedEndOfMailMessageException)
                        {
                            notEnd = false;
                        }
                    }
                }
            }
        }

        private IEnumerable<string> StringToLines(string s)
        {
            using (StringReader stringReader = new StringReader(s))
                while (stringReader.Peek() > 0)
                    yield return stringReader.ReadLine();
        }

        static public MailMessage FromEml(string eml)
        {
            MailMessageParse mailMessageParse = new MailMessageParse();
            mailMessageParse.ParseEml(eml);
            return mailMessageParse.MailMessage;
        }

        public void ParseFromStringEnumerator(IEnumerator<string> lines)
        {
            _mailMessage = new MailMessage();

            MailMessageReader mailMessageReader = new MailMessageReader(lines);
            mailMessageReader.UnknownHeader += MailMessageReaderUnknownHeader;
            mailMessageReader.InlineBody += MailMessageReaderInlineBody;
            mailMessageReader.Attachment += MailMessageReaderAttachment;
            mailMessageReader.Read();
        }

        public void ParseEml(string eml)
        {
            ParseFromStringEnumerator(StringToLines(eml).GetEnumerator());
        }

        void MailMessageReaderAttachment(Attachment attachment)
        {
            _mailMessage.Attachments.Add(attachment);
        }

        void MailMessageReaderInlineBody(string mediaType, string body)
        {
            _mailMessage.IsBodyHtml = mediaType == MediaTypeNames.Text.Html;
            _mailMessage.Body = body;
        }

        void MailMessageReaderUnknownHeader(string headerName, string headerValue)
        {
            switch (headerName.Trim().ToUpperInvariant())
            {
                case "FROM":
                    _mailMessage.Headers.Add(headerName, headerValue);
                    var fromAddress = new MailAddress(headerValue);
                    _mailMessage.From = new MailAddress(fromAddress.Address, Rfc.EncodedWord.Decode(fromAddress.DisplayName));
                    break;
                case "TO":
                    //_mailMessage.Headers.Add(headerName, headerValue);
                    //if (headerValue.IndexOf('@') != headerValue.LastIndexOf('@')) // e-mail`ов много
                    //    _mailMessage.To.Add(headerValue);
                    //else
                    //    _mailMessage.To.Add(new MailAddress(Parse(headerValue)));
                    break;
                case "SUBJECT":
                    _mailMessage.Headers.Add(headerName, headerValue);
                    _mailMessage.Subject = Rfc.EncodedWord.Decode(headerValue);
                    break;
                case "REPLY-TO":
                    _mailMessage.Headers.Add(headerName, headerValue);
                    var replyToAddress = new MailAddress(headerValue);
                    _mailMessage.ReplyTo = new MailAddress(replyToAddress.Address, Rfc.EncodedWord.Decode(replyToAddress.DisplayName));
                    break;
                default:
                    //_mailMessage.Headers.Add(headerName, headerValue);
                    break;
            }
        }

        
    }
}
