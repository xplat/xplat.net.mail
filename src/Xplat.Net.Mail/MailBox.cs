﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xplat.Net.Mail
{
    /// <summary>
    /// Information about mailbox
    /// </summary>
    public class Mailbox
    {
        /// <summary>
        /// Mailbox name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MailboxMode Mode { get; set; }

        /// <summary>
        /// Defined flags in the mailbox
        /// </summary>
        public List<string> Flags { get; set; }

        /// <summary>
        /// Defined permanent flags in the mailbox
        /// </summary>
        public List<string> PermanentFlags { get; set; }

        /// <summary>
        /// The number of messages in the mailbox.
        /// </summary>
        public long Exists { get; set; }
      
        /// <summary>
        /// The number of messages with the \Recent flag set
        /// </summary>
        public long Recent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? Unseen { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? UidValidity { get; set; }
    }
}
