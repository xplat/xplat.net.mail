﻿using System;
using System.Text;
using System.Globalization;
using System.Collections.Generic;

namespace Xplat.Net.Mail
{
    public class ImapSearch
    {
        public string From { get; set; }

        public string Subject { get; set; }

        public DateTime Before { get; set; }

        public DateTime After { get; set; }

        public DateTime On { get; set; }

        public string Charset { get; set; }

        public ImapSearch SUBJECT(string subject)
        {
            this.Subject = subject;
            return this;
        }

        public ImapSearch FROM(string from)
        {
            this.From = from;
            return this;
        }

        public ImapSearch BEFORE(DateTime before)
        {
            this.Before = before;
            return this;
        }

        public ImapSearch CHARSET(string charset)
        {
            this.Charset = charset;
            return this;
        }

        public ImapSearch SINCE(DateTime after)
        {
            this.After = after;
            return this;
        }

        public ImapSearch ON(DateTime on)
        {
            this.On = on;
            return this;
        }

        public ImapSearch SEEN()
        {
            isSeen = true;
            return this;
        }

        public ImapSearch UNSEEN()
        {
            isSeen = false;
            return this;
        }

        private bool? isSeen = null;

        public ImapSearch RECENT()
        {
            isRecent = true;
            return this;
        }

        private bool isRecent = false;

        public ImapSearch TEXT(string text)
        {
            this.Text = text;

            return this;
        }

        public string Text { get; set; }

        public override string ToString()
        {
            StringBuilder searchQueryBuilder = new StringBuilder();

            if (!string.IsNullOrEmpty(From))
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "FROM \"{0}\" ", From);
            if (default(DateTime) != Before)
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "BEFORE {0:dd-MMM-yyyy} ", Before);
            if (default(DateTime) != After)
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "SINCE {0:dd-MMM-yyyy} ", After);
            if (default(DateTime) != On)
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "ON {0:dd-MMM-yyyy} ", On);
            if (!string.IsNullOrEmpty(Subject))
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "SUBJECT \"{0}\" ", Subject);

            if (!string.IsNullOrEmpty(Text))
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "TEXT \"{0}\" ", Text);
            if (!string.IsNullOrEmpty(Body))
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "BODY \"{0}\" ", Body);

            if (isRecent)
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "RECENT ");
            if (isSeen.HasValue)
            {
                if (isSeen.Value)
                    searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "SEEN ");
                else
                    searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, "UNSEEN ");
            }
            return searchQueryBuilder.ToString().TrimEnd();
        }

        public ImapSearch BODY(string body)
        {
            this.Body = body;
            return this;
        }

        public string Body { get; set; }

        internal bool IsASCII()
        {
            string searchCriteria = ToString();
            return Encoding.UTF8.GetByteCount(searchCriteria) == searchCriteria.Length;
        }

        internal string[] GetNonASCII()
        {
            List<string> commands = new List<string>();

            StringBuilder searchQueryBuilder = new StringBuilder();
            searchQueryBuilder.Append("CHARSET UTF-8");

            if (!string.IsNullOrEmpty(Subject))
            {              
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " SUBJECT {{{0}+}}", Encoding.UTF8.GetBytes(Subject).Length);
                commands.Add(searchQueryBuilder.ToString());

                searchQueryBuilder = new StringBuilder();
                searchQueryBuilder.Append(Subject);
            }
            if (!string.IsNullOrEmpty(Text))
            {
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " TEXT {{{0}+}}", Encoding.UTF8.GetBytes(Text).Length);
                commands.Add(searchQueryBuilder.ToString());

                searchQueryBuilder = new StringBuilder();
                searchQueryBuilder.Append(Text);
            }
            if (!string.IsNullOrEmpty(Body))
            {
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " BODY {{{0}+}}", Encoding.UTF8.GetBytes(Body).Length);
                commands.Add(searchQueryBuilder.ToString());

                searchQueryBuilder = new StringBuilder();
                searchQueryBuilder.Append(Body);
            }

            if (!string.IsNullOrEmpty(From))
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " FROM \"{0}\"", From);
            if (default(DateTime) != Before)
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " BEFORE {0:dd-MMM-yyyy}", Before);
            if (default(DateTime) != After)
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " SINCE {0:dd-MMM-yyyy}", After);
            if (default(DateTime) != On)
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " ON {0:dd-MMM-yyyy}", On);
            

            if (isRecent)
                searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " RECENT");
            if (isSeen.HasValue)
            {
                if (isSeen.Value)
                    searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " SEEN");
                else
                    searchQueryBuilder.AppendFormat(CultureInfo.InvariantCulture, " UNSEEN");
            }

            commands.Add(searchQueryBuilder.ToString());

            return commands.ToArray();
        }
    }
}
