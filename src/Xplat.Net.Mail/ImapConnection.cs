﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Xplat.Net.Mail
{
    public class ImapConnection
    {
        private TcpClient _tcpClient;
        private Stream _writeStream;
        private StreamReader _readStream;

        public void Connect(string hostName, int port, bool useSsl)
        {
            _tcpClient = new TcpClient(hostName, port);

            _writeStream = _tcpClient.GetStream();
            if (useSsl)
            {

                SslStream sslStream = new SslStream(_writeStream, false, SertificateValidation);
                sslStream.AuthenticateAsClient(hostName);
                _writeStream = sslStream;
            }

            _readStream = new StreamReader(_writeStream, Encoding.ASCII);

            Read();
        }

        private bool SertificateValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;
            return false;
        }

        public void WriteLine(string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message + "\r\n");
            _writeStream.Write(data, 0, data.Length);
            OnMessageSend(message);
        }

        public void Read()
        {
            while (!_readStream.EndOfStream)
                ReadLine();
        }

        public string ReadLine()
        {
            string recivedMessage = _readStream.ReadLine();
            OnMessageRecived(recivedMessage);
            return recivedMessage;
        }

        public event Action<string> MessageSend;

        protected void OnMessageSend(string message)
        {
            if (MessageSend != null)
                MessageSend(message);
        }

        public event Action<string> MessageRecived;

        protected void OnMessageRecived(string message)
        {
            if (MessageRecived != null)
                MessageRecived(message);
        }

    }
}
