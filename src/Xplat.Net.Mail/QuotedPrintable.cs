﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Xplat.Net.Mail
{
    public static partial class Rfc
    {
        /// <summary>
        /// Used for decoding Quoted-Printable text.<br/>
        /// This is a robust implementation of a Quoted-Printable decoder defined in <a href="http://tools.ietf.org/html/rfc2045">RFC 2045</a>.<br/>
        /// Every measurement has been taken to conform to the RFC.
        /// </summary>
        public static class QuotedPrintable
        {
            public static string Decode(string quotedString, Encoding encoding)
            {
                return Regex.Replace(quotedString, "(?:=[0-9A-F]{2})+", quotedMatch =>
                {
                    byte[] data = new byte[quotedMatch.Length / 3];
                    for (int j = 0, i = 1; i < quotedMatch.Length; i += 3, j++)
                        data[j] = byte.Parse(quotedMatch.Value.Substring(i, 2), NumberStyles.HexNumber);
                    return encoding.GetString(data);
                }, RegexOptions.IgnoreCase);
            }
        }
    }
}
