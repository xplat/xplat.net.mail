﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xplat.Net.Mail.Tests
{
    [TestFixture]
    public class EncodedWordTests
    {
        [Test]
        public void TestSimpleBase64()
        {
            string name = Rfc.EncodedWord.Decode("=?windows-1251?B?xebl5O3l4u376SDw5eXx8vAg7+vg8uXm5ekgMjIuMDkuMjAxNQ==?=");

            Assert.AreEqual("Ежедневный реестр платежей 22.09.2015", name);
        }

        [Test]
        public void TestSmallLetterForEncoding()
        {
            string name = Rfc.EncodedWord.Decode("=?windows-1251?b?xebl5O3l4u376SDw5eXx8vAg7+vg8uXm5ekgMjIuMDkuMjAxNQ==?=");

            Assert.AreEqual("Ежедневный реестр платежей 22.09.2015", name);
        }

        [Test]
        public void TestUnderscoreInQuotedPrintable()
        {
            string name = Rfc.EncodedWord.Decode("=?ISO-8859-5?Q?=B7=B0=BE_=B8=DD=E4=DE=E0=DC=D0=E6=D8=DE=DD=DD=DE-=DF=E0=DE=E6=D5=E1=E1=D8=DD=D3=DE=D2=EB=D9_?==?ISO-8859-5?Q?=E6=D5=DD=E2=E0_=28=B1=B8=BB=B0=B9=BD_?==?ISO-8859-5?Q?-_=C1=BE=C2=BE=B2=B0=CF_=C1=B2=CF=B7=CC=29=5F20130730=5F180006.7z?=");

            Assert.AreEqual("ЗАО Информационно-процессинговый центр (БИЛАЙН - СОТОВАЯ СВЯЗЬ)_20130730_180006.7z", name);
        }

        [Test]
        public void TestSpacesBetweenEncodedWords()
        {
            string name = Rfc.EncodedWord.Decode("=?UTF-8?b?S09LS1/QldC00LjQvdCw0Y8=?= =?UTF-8?b?INCa0LDRgdGB0LBfMDYxMDIwMTUuQ1NW?=");

            Assert.AreEqual("KOKK_Единая Касса_06102015.CSV", name);
        }

        [Test]
        public void TestSubjectCp1251()
        {
            string subject = Rfc.EncodedWord.Decode("Subject: =?Cp1251?Q?P2P=5Fkz_=F0=E5=E5=F1=F2=F0_c_01.03.2016_=EF=EE_20.03.2016?=");

            Assert.AreEqual("Subject: P2P_kz реестр c 01.03.2016 по 20.03.2016", subject);
        } 
    }
}
